FROM registry.gitlab.com/affiliatelk/supercronic-ubuntu:latest

RUN apt-get update && \
    apt-get install -y python-software-properties software-properties-common && \
    LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php && \
    apt-get update && \
    apt-get install php7.0 && \
    apt-get --purge autoremove -y

# Ioncube loader
RUN apt-get update && \
    wget https://downloads.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz && \
    tar -zxvf ioncube_loaders_lin_x86-64.tar.gz && \
    mkdir /usr/local/ioncube && \
    cp ioncube/ioncube_loader_lin_7.0.so /usr/local/ioncube/ioncube_loader_lin_7.0.so && \
    echo "zend_extension = /usr/local/ioncube/ioncube_loader_lin_7.0.so" >> /usr/local/etc/php/php.ini && \
    rm ioncube_loaders_lin_x86-64.tar.gz && \
    rm -R ioncube

ADD crontab /root/platform-cron
WORKDIR /opt
ENTRYPOINT ["./supercronic"]
CMD ["./platform-cron"]